package com.indonesia.ridwan.mengirimdatadariactivitysatukeactivitylain;


import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;

public class MainActivity extends AppCompatActivity implements OnClickListener{

    //mendeklarasiakan Variable

    Button button;
    RadioGroup genderRadioGroup;
    EditText nama;
    EditText umur;


    /*Memanggil activity petama du buat*/
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //Menampilkan semnua tampilan id
        findAllViewsId();

        button.setOnClickListener(this);
    }

    private void findAllViewsId(){
        button = (Button) findViewById(R.id.kirimdata);
        nama = (EditText) findViewById(R.id.nama);
        umur = (EditText) findViewById(R.id.umur);
        genderRadioGroup = (RadioGroup) findViewById(R.id.jeniskelamin);


    }

    @Override
    public void onClick(View v) {
        Intent intent = new Intent(getApplicationContext(),MainActivity2.class);

        //membuat object bundle
        Bundle b = new Bundle();

        //Menyisipkan tipe data String ke dalam object bundle
        b.putString("nama", nama.getText().toString());
        b.putString("umur" , umur.getText().toString());
        int id = genderRadioGroup.getCheckedRadioButtonId();
        RadioButton radioButton = (RadioButton) findViewById(id);
        b.putString("jenis Kelamin", radioButton.getText().toString());

        //menambah bundle intent
        intent.putExtras(b);

        //memnulai Activity kedua
        startActivity(intent);
    }
}
