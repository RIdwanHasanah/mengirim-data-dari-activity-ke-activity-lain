package com.indonesia.ridwan.mengirimdatadariactivitysatukeactivitylain;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.widget.TextView;

/**
 * Created by hasanah on 6/30/16.
 */
public class MainActivity2 extends AppCompatActivity {

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_2);


        Bundle b = getIntent().getExtras();
        //membuat object dari widget textview
        TextView nama = (TextView) findViewById(R.id.namaValue);
        TextView umur = (TextView) findViewById(R.id.umurValue);
        TextView jeniskelamin = (TextView) findViewById(R.id.jeniskelaminValue);

        //men-set nilai dari widget textView
        nama.setText(b.getCharSequence("nama"));
        umur.setText(b.getCharSequence("umur"));
        jeniskelamin.setText(b.getCharSequence("jeniskelamin"));

    }
}
